package br.com.bry.framework.sample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.bouncycastle.cms.CMSException;

import br.com.bry.framework.sample.config.CertificateConfig;
import br.com.bry.framework.sample.config.ServiceConfig;
import br.com.bry.framework.sample.config.SignatureConfig;
import br.com.bry.framework.sample.enums.HashAlgorithm;
import br.com.bry.framework.sample.util.CMSUtil;
import br.com.bry.framework.sample.util.ConverterUtil;
import br.com.bry.framework.sample.util.FileUtil;
import br.com.bry.framework.sample.util.PKCS1DTO;
import br.com.bry.framework.sample.util.Signer;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BasicSignatureExample {

	public static void main(String[] args) throws IOException, JSONException, NoSuchAlgorithmException, CMSException {

		// Step 1 - Load PrivateKey and Certificate
		Signer signer = new Signer(CertificateConfig.PRIVATE_KEY_LOCATION, CertificateConfig.PRIVATE_KEY_PASSWORD);

		// Step 2 - Generate hash hex string from documents
		List<String> hashOriginalDocumentsList = generateHashesFromOriginalDocuments();

		// Step 3 - Signature initialization.
		List<PKCS1DTO> initializedData = initializeSignature(signer, hashOriginalDocumentsList);

		// Step 4 - Local encryption of signed attributes using private key.
		encryptSignedAttributes(signer, initializedData);

		// Step 5 - Signature finalization.
		List<String> signatureContent = finalizeSignature(signer, initializedData);

		for (int i = 0; i < signatureContent.size(); i++)
			FileUtil.writeContentToFile(SignatureConfig.OUTPUT_RESOURCE_FOLDER, "signature-item-" + i, signatureContent.get(0));
		
		// Step 6 (Optional, use only if required) - Local signature attachment.
		//attachSignatures(signatureContent);
	}

	private static List<String> generateHashesFromOriginalDocuments() throws NoSuchAlgorithmException, IOException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		List<String> originalDocumentsLocation = SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION;
		List<String> hashOriginalDocumentsList = new ArrayList<String>();
		for (String pathDocument : originalDocumentsLocation) {
			File file = new File(pathDocument);
			byte[] bytes = Files.readAllBytes(file.toPath());
			byte[] encodedhash = md.digest(bytes);
			String encodeHexString = Hex.encodeHexString(encodedhash);
			hashOriginalDocumentsList.add(encodeHexString);
		}
		return hashOriginalDocumentsList;
	}

	private static List<PKCS1DTO> initializeSignature(Signer signer, List<String> hashOriginalDocumentsList) throws IOException, JSONException {

		RequestSpecBuilder builder = new RequestSpecBuilder();

		IntStream.range(0, SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION.size()).forEach(i -> {
			builder.addMultiPart("originalDocuments[" + i + "][nonce]", String.valueOf(System.currentTimeMillis()));
			builder.addMultiPart("originalDocuments[" + i + "][hash]", hashOriginalDocumentsList.get(i));
		});

		RequestSpecification requestSpec = builder.build();

		String certificateBase64Content = Base64.getEncoder().encodeToString(signer.getCertificate());

		Response initializationResponse = RestAssured.given().auth().preemptive().oauth2(ServiceConfig.ACCESS_TOKEN).spec(requestSpec).multiPart("nonce", System.currentTimeMillis())
				.multiPart("attached", SignatureConfig.ATTACHED).multiPart("profile", SignatureConfig.PROFILE).multiPart("hashAlgorithm", SignatureConfig.HASH_ALGORITHM)
				.multiPart("certificate", certificateBase64Content).multiPart("operationType", SignatureConfig.OPERATION_TYPE).expect().when().post(ServiceConfig.URL_INITIALIZE_SIGNATURE);

		Object responseBody = initializationResponse.getBody().as(Object.class);
		if (initializationResponse.getStatusCode() != 200) {
			System.out.println("Error during signature initialization - Status code: " + initializationResponse.getStatusCode());
			System.out.println(responseBody);
			throw new IOException("Error during signature initialization - Signature initialization aborted.");
		}

		System.out.println("Signature initialization JSON response: " + responseBody);

		JSONObject jsonObject = new JSONObject(ConverterUtil.convertObjectToJSON(responseBody));

		JSONArray signedAttributes = jsonObject.getJSONArray("signedAttributes");

		List<PKCS1DTO> data = new ArrayList<>();
		for (int i = 0; i < signedAttributes.length(); i++) {
			String signedAttributesStringJson = signedAttributes.getString(i);

			JSONObject signedAttributesJsonObject = new JSONObject(signedAttributesStringJson);

			PKCS1DTO dado = new PKCS1DTO(signedAttributesJsonObject.getString("nonce"), signedAttributesJsonObject.getString("content"));
			dado.setHashOriginalDocument(hashOriginalDocumentsList.get(i));
			data.add(dado);
		}

		return data;

	}

	private static void encryptSignedAttributes(Signer signer, List<PKCS1DTO> signedAttribute) throws IOException {

		for (PKCS1DTO dado : signedAttribute) {
			byte[] signatureValue = signer.sign(HashAlgorithm.valueOf(SignatureConfig.HASH_ALGORITHM), Base64.getDecoder().decode(dado.getSignedAttribute()));
			dado.setSignatureValue(Base64.getEncoder().encodeToString(signatureValue));
		}

	}

	private static List<String> finalizeSignature(Signer signer, List<PKCS1DTO> listOfSignatureValues) throws IOException, JSONException {
		RequestSpecBuilder builder = new RequestSpecBuilder();

		IntStream.range(0, listOfSignatureValues.size()).forEach(i -> {
			PKCS1DTO pkcs1Item = listOfSignatureValues.get(i);
			builder.addMultiPart("finalizations[" + i + "][nonce]", pkcs1Item.getNonce());
			builder.addMultiPart("finalizations[" + i + "][signedAttributes]", pkcs1Item.getSignedAttribute());
			builder.addMultiPart("finalizations[" + i + "][signatureValue]", pkcs1Item.getSignatureValue());
			builder.addMultiPart("finalizations[" + i + "][hash]", pkcs1Item.getHashOriginalDocument());
		});

		RequestSpecification requestSpec = builder.build();

		String certificateBase64Content = Base64.getEncoder().encodeToString(signer.getCertificate());

		Response finalizationResponse = RestAssured.given().auth().preemptive().oauth2(ServiceConfig.ACCESS_TOKEN).spec(requestSpec).multiPart("nonce", System.currentTimeMillis())
				.multiPart("attached", SignatureConfig.ATTACHED).multiPart("profile", SignatureConfig.PROFILE).multiPart("hashAlgorithm", SignatureConfig.HASH_ALGORITHM)
				.multiPart("certificate", certificateBase64Content).multiPart("operationType", SignatureConfig.OPERATION_TYPE).expect().when().post(ServiceConfig.URL_FINALIZE_SIGNATURE);

		Object responseBody = finalizationResponse.getBody().as(Object.class);
		if (finalizationResponse.getStatusCode() != 200) {

			System.out.println("Error during signature finalization - Status code: " + finalizationResponse.getStatusCode());
			System.out.println(responseBody);
			throw new IOException("Error during signature finalization - Signature finalization aborted.");
		}

		System.out.println("Signature finalization JSON response: " + responseBody);

		JSONObject jsonObjectFinalization = new JSONObject(ConverterUtil.convertObjectToJSON(responseBody));

		JSONArray signatureArray = jsonObjectFinalization.getJSONArray("signatures");

		List<String> generatedSignatures = new ArrayList<>();
		for (int i = 0; i < signatureArray.length(); i++) {
			String signaturesStringJson = signatureArray.getString(i);
			JSONObject signaturesJsonObject = new JSONObject(signaturesStringJson);
			generatedSignatures.add(signaturesJsonObject.getString("content"));
		}

		return generatedSignatures;
	}

	private static void attachSignatures(List<String> signatureContent) throws IOException, CMSException {
		// Get original documents location
		List<String> originalDocumentsLocation = SignatureConfig.ORIGINAL_DOCUMENTS_LOCATION;
		if (signatureContent.size() != originalDocumentsLocation.size()) {
			throw new IOException("Number of signatures is different from original documents");
		}
		
		// For each signature, attach original document to it
		for (int i = 0; i < signatureContent.size(); i++) {
			// Generate Output file for attached signature
			String completeAttachedSignatureFilePath = new StringBuilder()
					.append(SignatureConfig.OUTPUT_RESOURCE_FOLDER)
					.append("attached-signature-item-")
					.append(i)
					.append(new SimpleDateFormat("_yyyy-MM-dd hh.mm.ss.SSS").format(new Date()))
					.append(".p7s")
					.toString();
			File completeAttachedSignatureFile = FileUtil.createFile(completeAttachedSignatureFilePath);				
			
			// Get bytes of signature
			String signatureContentString = signatureContent.get(i);
			byte[] signatureContentBytes = Base64.getDecoder().decode(signatureContentString);
			
			// Get InputStream of original document
			String originalDocumentLocation = originalDocumentsLocation.get(i);
			File originalDocumentFile = new File(originalDocumentLocation);
			
			// Attach signature
			try (InputStream originalDocumentStream = new FileInputStream(originalDocumentFile); 
					OutputStream attachedSignatureOutput = new FileOutputStream(completeAttachedSignatureFile)) {
				CMSUtil.attachSignature(signatureContentBytes, originalDocumentStream, attachedSignatureOutput);
			}
			System.out.println("Successful attached signature generation: " + completeAttachedSignatureFilePath);
		}		
	}
}
