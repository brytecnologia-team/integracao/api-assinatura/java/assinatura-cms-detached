package br.com.bry.framework.sample.util;

import br.com.bry.framework.sample.config.SignatureConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

public class FileUtil {
	
	public static void writeContentToFile(String filePath, String fileName, String content) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("_yyyy-MM-dd hh.mm.ss.SSS");

		String signatureFullName = filePath + fileName + formatter.format(new Date(System.currentTimeMillis())) + ".p7s";
		File f = createFile(signatureFullName);
		
		FileOutputStream in = new FileOutputStream(f);
		in.write(Base64.getDecoder().decode(content));
		in.close();

		System.out.println("Successful signature generation: " + SignatureConfig.OUTPUT_RESOURCE_FOLDER + signatureFullName);
		
	}
	
	public static File createFile(String completeFileName) throws IOException {
		File newFile = new File(completeFileName);
		if (!newFile.createNewFile())
			throw new IOException("File already created");
		
		return newFile;
	}
	
}
