package br.com.bry.framework.sample.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataStreamGenerator;
import org.bouncycastle.util.Store;

public class CMSUtil {
	private CMSUtil() {}
	

	public static byte[] attachSignature(byte[] signature, InputStream originalDocument) throws CMSException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		attachSignature(signature, originalDocument, baos);
		
		return baos.toByteArray();
	}
	
	public static void attachSignature(byte[] signature, InputStream originalDocument, OutputStream attachedSignature) throws CMSException, IOException {
		// Create signed data object from signature
		CMSSignedData signedData = new CMSSignedData(signature);
		
		// Create new signed data generator
		CMSSignedDataStreamGenerator signedDataGenerator = new CMSSignedDataStreamGenerator();
		
		// Add signature data to new generator
		signedDataGenerator.addSigners(signedData.getSignerInfos());
		signedDataGenerator.addCertificates(signedData.getCertificates());
		
		// Add CRLs to new generator
		Store crlsStore = signedData.getCRLs();
		if (crlsStore != null) {
			signedDataGenerator.addCRLs(crlsStore );
		}
		
		// Add OCSP info
		Store ocspInfo = signedData.getOtherRevocationInfo(CMSObjectIdentifiers.id_ri_ocsp_response);
		if (ocspInfo != null) {
			signedDataGenerator.addOtherRevocationInfo(CMSObjectIdentifiers.id_ri_ocsp_response, ocspInfo);
		}
		
		// Create signed data object with attached property
		try (OutputStream signedDataOutputStream = signedDataGenerator.open(attachedSignature, true)) {
			// Write originalDocument into attachedSignature
			byte[] buffer = new byte[4096];
			while(originalDocument.available() > 0) {
				int bytesReaded = originalDocument.read(buffer);
				if (bytesReaded <= 0) {
					break;
				}
				signedDataOutputStream.write(buffer, 0, bytesReaded);
			}	
		}
		attachedSignature.flush();
	}
}
