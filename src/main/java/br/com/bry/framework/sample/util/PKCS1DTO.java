package br.com.bry.framework.sample.util;

public class PKCS1DTO {

	private String nonce;
	private String signedAttribute;
	private String signatureValue;
	private String hashOriginalDocument;

	public PKCS1DTO(String nonce, String signedAttribute) {
		this.nonce = nonce;
		this.signedAttribute = signedAttribute;
	}

	public void setSignatureValue(String signatureValue) {
		this.signatureValue = signatureValue;
	}

	public String getNonce() {
		return nonce;
	}

	public String getSignedAttribute() {
		return signedAttribute;
	}

	public String getSignatureValue() {
		return signatureValue;
	}

	public String getHashOriginalDocument() {
		return hashOriginalDocument;
	}

	public void setHashOriginalDocument(String hashOriginalDocument) {
		this.hashOriginalDocument = hashOriginalDocument;
	}

}
